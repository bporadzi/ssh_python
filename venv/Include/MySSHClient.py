import threading
import time
import paramiko
import json
import os


class MySSHClient:
    FULL_DATE_LENGTH = 12

    MONTHS = {
        "Jan": "01",
        "Feb": "02",
        "Mar": "03",
        "Apr": "04",
        "May": "05",
        "Jun": "06",
        "Jul": "07",
        "Aug": "08",
        "Sep": "09",
        "Oct": "10",
        "Nov": "11",
        "Dec": "12"
    }

    def __init__(self):
        with open("config.txt") as file:
            data = json.load(file)

        self.__local_folder = data["local_folder"]
        self.__remote_folder = data["remote_folder"]
        self.__server_address = data["server_address"]
        self.__port = data["port"]
        self.__username = data["username"]
        self.__mode = data["mode"]  # overwrite/update/add_non_existing
        self.__ignore = data["ignore"]  # "bmp"

        self.__password = input("Input password for user '" + self.__username + "'\n")

        threading.Thread(self.run())

    def run(self):
        print("synchronizing folder '" + self.__local_folder + "' with remote folder '" + self.__remote_folder + "'")

        try:
            self.__ssh_client = paramiko.SSHClient()
            self.__ssh_client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
            self.__ssh_client.connect(self.__server_address, self.__port, self.__username, self.__password)

            self.__ssh_client.exec_command("mkdir " + self.__remote_folder)
            synchronized_files_list = self.getSynchronizedFilesList()

            try:
                ftp_client = self.__ssh_client.open_sftp()

                for i in range(0, len(synchronized_files_list)):
                    path_to_local_file = self.__local_folder + "/" + synchronized_files_list[i]
                    path_to_remote = self.__remote_folder + "/" + synchronized_files_list[i]

                    ftp_client.put(path_to_local_file, path_to_remote)

                    modification_time = time.strftime("%Y%m%d%H%M.%S",
                                                      time.localtime(os.path.getmtime(path_to_local_file)))

                    self.__ssh_client.exec_command(
                        "touch -m -a -t " + modification_time + " " + path_to_remote)

                    print("successfully synchronized file " + synchronized_files_list[i])
            finally:
                ftp_client.close()

        except Exception as ex:
            print("error")
            pass
        finally:
            self.__ssh_client.close()

    def getSynchronizedFilesList(self):
        if (self.__mode == "overwrite"):
            return self.getLocalFilesList()

        synchronized_files = []

        stdin, stdout, stderr = self.__ssh_client.exec_command("ls " + self.__remote_folder + "/")

        remote_files = self.trim(stdout.readlines())

        ftp_client = self.__ssh_client.open_sftp()

        if (self.__mode == "add_non_existing"):
            try:
                local_files = self.getLocalFilesList()

                for i in range(0, len(local_files)):
                    is_on_remote = False

                    for j in range(0, len(remote_files)):
                        if (local_files[i] == remote_files[j]):
                            is_on_remote = True
                    if (not is_on_remote):
                        synchronized_files.append(local_files[i])
            finally:
                ftp_client.close()
                return synchronized_files
        if (self.__mode == "update"):
            try:
                for i in range(0, len(remote_files)):
                    worth_to_check_date = True  # variable and loop for checking if file on remote is not excluded in config file

                    for k in range(0, len(self.__ignore)):
                        if (remote_files[i].endswith(self.__ignore[k])):
                            worth_to_check_date = False
                            break

                    if not worth_to_check_date:
                        continue

                    stdin, stdout, stderr = self.__ssh_client.exec_command(
                        "ls -lt " + self.__remote_folder + "/" + remote_files[i])

                    remote_modification_date = self.parseLine(stdout.readlines())

                    local_file_path = self.__local_folder + "/" + remote_files[i]

                    local_modification_time = time.strftime("%Y%m%d%H%M",
                                                            time.localtime(os.path.getmtime(local_file_path)))

                    if (os.path.isfile(local_file_path)):
                        if (len(
                                remote_modification_date) != MySSHClient.FULL_DATE_LENGTH or remote_modification_date != local_modification_time):  # synchronizes also files from last year
                            print("file " + remote_files[i] + " will be updated")
                            synchronized_files.append(remote_files[i])
            except Exception as e:
                print(e)
            finally:
                ftp_client.close()
                return synchronized_files

        return synchronized_files

    def parseLine(self, line):
        result = str(line)
        # 43 begin index of line on sendzimir.metal.agh.edu.pl

        month = MySSHClient.MONTHS[result[43: 46]]
        day = result[47: 49]

        fragment_with_hour = result[50:54]

        this_year = fragment_with_hour.find(":") != -1

        if (this_year):
            hour = result[50:52]
            minute = result[53:55]
            year = "2018"

            return year + month + day + hour + minute
        else:
            year = result[51:55]
            return year + month + day

        return

    def trim(self, files):
        remote_files = []

        for i in range(0, len(files)):
            remote_files.append(files[i][:len(files[i]) - 1])

        return remote_files

    def getLocalFilesList(self):
        all_files_list = os.listdir(self.__local_folder)

        local_good_files_list = []

        synchronized_files_list = []

        for i in range(0, len(all_files_list)):
            good_file = True
            for j in range(0, len(self.__ignore)):
                if (all_files_list[i].endswith(self.__ignore[j])):
                    good_file = False
                    break
            if (good_file):
                synchronized_files_list.append(all_files_list[i])
        return synchronized_files_list
